Cat-Avatar-Generator
====================

![cover picture](http://www.peppercarrot.com/data/images/lab/2016-11-30_cdn/2016-11-29_the-quest-to-free-peppercarrot-website_02a-avatar.jpg)

A generator of avatar images. Can either generate a random avatars, or a specific avatar from a "seed".
A derivation by [David Revoy](http://www.peppercarrot.com) from the original [MonsterID by Andreas Gohr's](https://www.splitbrain.org/blog/2007-01/20_monsterid_as_gravatar_fallback).

## License

**Artwork:**
All image files are licensed under: [CC-By 4.0](https://creativecommons.org/licenses/by/4.0/) attribution: David Revoy with the following exception: Generated cats used as Avatar (for blog,forum,social-network) don't need direct attribution and so, can be used as regular avatars without pasting David Revoy's name all over the place.

**Code**
Both PHP and Python scripts are licensed under the short and simple permissive:
[MIT License](https://en.wikipedia.org/wiki/MIT_License)

## Usage

### PHP

The following PHP files are provided in this repository:

- `src/Generator.php` contains the `Generator` class which encapsulates avatar generation logic
- `src/GeneratorException.php` is a dummy Exception class, which is thrown whenever something bad happens in the generator.
- `public/avatar.php` is a callable script, which uses the `Generator` class for avatar generation, but also takes care of caching generated avatars and returning image stream to the client.
  The constants defined at the top of this file are configuration. Feel free to modify them as necessary.
  Three query string arguments are accepted by this file:
  - `size`: avatar edge size in pixels (a number between 1 and 400)
  - `seed`: the seed (the name for which you're generating the avatar)
  - `type`: avatar type (must match one of the subdirectory names under `avatars/`, see **Artwork directory structure** below)
- `public/index.php` is a simple demo page or playing with the generator interactively.

Ideally, only the files under `public/` should be publicly accessible, whereas all other files should not.

To get the image via HTTP, call the script this way:
```
echo '<img height="70" width="70" src="your/path/to/avatar.php?seed='.$var.'&size=70"/>';
```

_(Note: for the seed, I advise to use author's name to not expose email or sensitive data, even hashed on a public code.)_

If you want to generate avatars directly from your PHP code though, you may install it as a Composer library and call the generator class directly.

In this case you're advised to also implement image caching to prevent same images from being regenerated.

### Python

The Python version may be used to generate either cats or birds avatars by importing the `avatar_generator` module.

Here is a sample usage for this generation:

```python
from avatar_generator import get_cat, get_bird, get_avatar

# Creates a cat avatar
img1 = get_cat('Hello world')
img1.show()

# Creates a bird avatar
img2 = get_bird('Hello world')
img2.show()

# Creates a cat avatar with white background
img3 = get_avatar('cat', 'another seed', transparent=False)
img3.show()
```

Note: the Python version of the generator is somewhat outdated.
It doesn't read avatar metadata file and instead uses hardcoded values.
For the same reasons, it doesn't support abstract and mobilizon avatars yet.
It should be easy to fix though!

## Artwork directory structure

The generator builds avatars from pre-made components.
The path to the component directory is passed in its constructor argument.
The directory passed must contain the `avatar_metadata.json` file as well as all images.
The metadata file specifies the source size of the images (in pixels) as well as the list of layers with counts of templates for each layer. For example:

```json
{
  "size": 256,
  "layers": {
    "body": 15,
    "fur": 10,
    "eyes":  15,
    "mouth": 10
  }
}
```

The layers will be added to the avatar in the order they are listed here, and the number after the colon refers to the number of images available for that layer. 
The image files themselves must be named according to the following scheme: `LAYERNAME_NUMBER.png`.
Here, `LAYERNAME` is the name of the layer as defined in the JSON file, and `NUMBER` is a unique number starting with `1` and going up.
The numbers must be contiguous and must not be padded with zeroes.

The JSON structure allows easily adding more fields in the future, such as licensing and authorship info, for example.

## Editing existing artwork

1. Artwork sources can be found under `artwork-src/`. Open the appropriate source file in Krita (or Gimp, Mypaint, Pinta). Make your changes or additions, respect layer naming, then save.
2. Open it again in Gimp 2.8, with the [export layer plugin](https://github.com/khalim19/gimp-plugin-export-layers/releases/download/2.4/export-layers-2.4.zip)
3. Scale the image down to the result you want (e.g. 256px × 256px as on the demo ) LancZos filter
4. File > Export layer (Allow invisible layer to be exported, check 'image size', PNG file format )
5. Done. 

All PNG files of 'parts' are extracted this way and keep their layer name.
Note: some source files might contain incorrectly named layers.
Fixing these source files should be considered an entry in our TODO list.

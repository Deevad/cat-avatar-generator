<?php

namespace Revoy\AvatarGenerator;

use RuntimeException;

class GeneratorError extends RuntimeException
{
}
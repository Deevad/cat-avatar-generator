<?php

declare(strict_types=1);

namespace Revoy\AvatarGenerator;

use GdImage;
use Throwable;

class Generator
{
    private const DEFAULT_AVATAR_SIZE = 70;

    private readonly string $artworkPath;

    private readonly int $avatarSourceSize;

    private readonly array $avatarLayerInfo;

    /**
     * @param string $artworkPath path to the directory with avatar layer images and metadata file.
     */
    function __construct(string $artworkPath)
    {
        $this->artworkPath = rtrim($artworkPath, '/') . '/';

        $avatarMetadata = $this->getAndValidateAvatarMetadata();

        $this->avatarSourceSize = (int) $avatarMetadata['size'];
        $this->avatarLayerInfo = $avatarMetadata['layers'];
    }

    /**
     * Read the json metadata for avatar layers directory.
     *
     * @throws GeneratorError if anything unexpected happens.
     */
    private function getAndValidateAvatarMetadata(): array
    {
        try {
            $metadata = json_decode(file_get_contents($this->artworkPath . 'avatar_metadata.json'), true);
        } catch (Throwable $e) {
            throw new GeneratorError('Could not read or parse avatar type metadata.');
        }

        if (!isset($metadata['size']) || empty($metadata['layers'])) {
            throw new GeneratorError('Avatar metadata incomplete.');
        }

        if (!is_int($metadata['size']) || $metadata['size'] < 1) {
            throw new GeneratorError('Avatar size must be a positive integer.');
        }

        if (!is_array($metadata['layers']) || $metadata['layers'] === []) {
            throw new GeneratorError('Avatar layers list must be a non-empty array.');
        }

        foreach($metadata['layers'] as $count) {
            if (!is_int($count)) {
                throw new GeneratorError('Avatar layer list must be an array of string->integer values.');
            }
        }

        return $metadata;
    }

    /**
     * Generates a pseudo-random avatar with given parameters.
     *
     * @throws GeneratorError if anything unexpected happens.
     */
    private function buildAvatar(string $seed = '', int $size = self::DEFAULT_AVATAR_SIZE): GdImage
    {
        if ($size < 1) {
            throw new GeneratorError('Invalid size specified.');
        }

        $sourceSize = (int) $this->avatarSourceSize;

        if ($sourceSize <= 0) {
            throw new GeneratorError('Invalid avatar source image size in metadata.');
        }

        // Init random seed.
        if ($seed !== '') {
            srand(hexdec(substr(md5($seed), 0, 6)));
        }

        // Throw the dice for body layers.
        $layers = array_map(function ($imageCount) {
            return rand(1, $imageCount);
        }, $this->avatarLayerInfo);

        // Create composite avatar image.
        try {
            $avatar = imagecreatetruecolor($sourceSize, $sourceSize);

            if ($avatar === false) {
                throw new GeneratorError('imagecreatetruecolor() returned false.');
            }
        } catch (Throwable $e) {
            throw new GeneratorError("GD image creation failed", previous: $e);
        }

        $white = imagecolorallocate($avatar, 255, 255, 255);
        imagefill($avatar, 0, 0, $white);

        // Add layers.
        foreach ($layers as $layer => $imageNum) {
            $file = $this->artworkPath . $layer . '_' . $imageNum . '.png';

            try {
                $layerImage = imagecreatefrompng($file);

                if ($layerImage === false) {
                    throw new GeneratorError('imagecreatefrompng() returned false.');
                }
            } catch (Throwable $e) {
                throw new GeneratorError('Failed to load avatar layer file ' . $file . '.', previous: $e);
            }

            imagesavealpha($layerImage, true);
            imagecopy($avatar, $layerImage, 0, 0, 0, 0, $sourceSize, $sourceSize);
            imagedestroy($layerImage);
        }

        // restore random seed
        if ($seed) {
            srand();
        }

        if ($size === $sourceSize) {
            return $avatar;
        }

        try {
            $resizedAvatar = imagecreatetruecolor($size, $size);

            if ($resizedAvatar === false) {
                throw new GeneratorError('imagecreatetruecolor() returned false.');
            }
        } catch (Throwable $e) {
            throw new GeneratorError("GD resized image creation failed", previous: $e);
        }

        $success = imagecopyresampled($resizedAvatar, $avatar, 0, 0, 0, 0, $size, $size, $sourceSize, $sourceSize);
        imagedestroy($avatar);

        if ($success === false) {
            throw new GeneratorError("Could not resample the image.");
        }

        return $resizedAvatar;
    }

    public function buildPng(
        string $seed = '',
        int $size = self::DEFAULT_AVATAR_SIZE,
    ): string {
        $avatar = $this->buildAvatar($seed, $size);
        ob_start();
        imagepng($avatar);
        imagedestroy($avatar);
        $avatar = ob_get_clean();

        if ($avatar === false) {
            throw new GeneratorError('Unable to get avatar bytes.');
        }

        return $avatar;
    }

    public function buildJpeg(
        string $seed = '',
        int $size = self::DEFAULT_AVATAR_SIZE,
        int $quality = 90,
    ): string {
        $avatar = $this->buildAvatar($seed, $size);
        ob_start();
        imagejpeg($avatar, quality: $quality);
        imagedestroy($avatar);
        $avatar = ob_get_clean();

        if ($avatar === false) {
            throw new GeneratorError('Unable to get avatar bytes.');
        }

        return $avatar;
    }
}
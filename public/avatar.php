<?php

declare(strict_types=1);

use Revoy\AvatarGenerator\Generator;

require dirname(__DIR__) . '/src/Generator.php';
require dirname(__DIR__) . '/src/GeneratorError.php';

// /!\ change the path to your system's cache or a folder(write permission)
// Note: this path end with / and is relative to the cat-avatar-generator.php file.
const CACHE_PATH = __DIR__ . '/../cache/';

// This must point to the directory containing all avatar part sets.
const AVATAR_PATH = __DIR__ . '/../avatars/';

// Specify default avatar type to use if type has not been specified.
const DEFAULT_AVATAR_TYPE = 'cat';

const DEFAULT_AVATAR_SIZE = 70;

const MAX_AVATAR_SIZE = 400;

const MIN_AVATAR_SIZE = 12;

# Avatar cache time in seconds
const SERVER_CACHE_TIME = 604800; # 1 week (1 day = 86400)

const CLIENT_CACHE_TIME = 86400;

$avatarType = isset($_REQUEST['type'])
    ? preg_replace('/[^a-zA-Z0-9]/', '', $_REQUEST['type'])
    : DEFAULT_AVATAR_TYPE;

$avatarPath = AVATAR_PATH . $avatarType;

if (!is_dir($avatarPath)) {
    $avatarType = DEFAULT_AVATAR_TYPE;
    $avatarPath = AVATAR_PATH . $avatarType;
}

$avatarSize = isset($_REQUEST['size']) && is_numeric($_REQUEST['size'])
    ? (int)$_REQUEST['size']
    : DEFAULT_AVATAR_SIZE;

if ($avatarSize < MIN_AVATAR_SIZE) {
    $avatarSize = MIN_AVATAR_SIZE;
}

if ($avatarSize > MAX_AVATAR_SIZE) {
    $avatarSize = MAX_AVATAR_SIZE;
}

$headers = [
    'Pragma: public',
    'Cache-Control: max-age=' . CLIENT_CACHE_TIME,
    'Expires: ' . gmdate('D, d M Y H:i:s \G\M\T', time() + CLIENT_CACHE_TIME),
    'Content-Type: image/png',
];

$seed = $_GET['seed'] ?? '';
$cacheFileName = $seed === ''
    ? null
    : CACHE_PATH . $avatarType . '_' . hash('sha256', $seed) . '_' . $avatarSize . '.jpeg';

if ($seed !== '' && file_exists($cacheFileName) && time() - SERVER_CACHE_TIME <= filemtime($cacheFileName)) {
    foreach ($headers as $header) {
        header($header);
    }

    readfile($cacheFileName);

    exit;
}

$builder = new Generator($avatarPath);
$avatar = $builder->buildPng($seed, $avatarSize);

if ($cacheFileName !== null) {
    file_put_contents($cacheFileName, $avatar);
}

foreach ($headers as $header) {
    header($header);
}

exit($avatar);
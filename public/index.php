<?php
# Get var
$seed = $_GET['seed'] ?? '';
if ($seed == '') {
    $seed = uniqid();
}

$type = $_GET['type'] ?? 'cat';
$allowed_types = ['cat', 'bird', 'abstract', 'mobilizon'];

if (!in_array($type, $allowed_types)) {
    $type = 'cat';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Avatar generator</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Language" content="en"/>
    <link rel="shortcut icon" href="favicon.png" type="image/png"/>

    <style type="text/css" media="screen">body {
            margin: 2em;
            padding: 0;
            background: #a5c2e1;
            color: #666;
            font-size: 1rem;
            text-align: center;
        }

        #wrapper {
            text-align: left;
            background: #fff;
            max-width: 360px;
            padding: 1rem;
            margin: 0 auto;
            border: 1em #c6efff solid;
            text-align: center;
        }

        h1 {
            font-variant: small-caps;
            color: #dd3e00;
            font-size: 1.8rem;
            font-family: Ubuntu, Arial, sans;
            font-weight: bold;
            margin: 0 0 0.3rem 0;
        }

        .avatar {
            border: 0;
            margin: 2rem 0 0 0;
        }

        a {
            color: #c26639;
        }

        a:hover {
            color: #222;
        }

        .smallbutton {
            font-size: 1rem;
            margin-bottom: 0.8rem;
        }

        input[type=text] {
            width: 90%
        }

        .bigbutton {
            font-weight: bold;
            font-size: 1.3rem;
            margin-bottom: 1.3rem;
        }

        @media (max-width: 500px) {
            body {
                margin: 0;
                padding: 0;
                background: #FFF;
                font-size: 1rem;
            }

            #wrapper {
                text-align: left;
                background: #fff;
                max-width: 100%;
                padding: 0;
                margin: 0 auto;
                border: none;
                text-align: center;
            }

            h1 {
                font-size: 1.6rem;
                margin: 0.2rem 0 0.2rem 0;
            }
        }
    </style>
</head>
<body>
<div id="wrapper">
    <h1>Avatar generator</h1>

    <img class="avatar"
         src="avatar.php?seed=<?= htmlspecialchars($seed) ?>&amp;type=<?= htmlspecialchars($type) ?>&amp;size=255"
         title="To download the image, right click and choose &#34;Save Image As&#34;" alt="Your avatar">
    <br/>
    <strong>
        ~
        <?= htmlspecialchars(mb_strlen($seed) <= 25 ? $seed : mb_substr($seed, 0, 25) . '…') ?>
        ~
    </strong>

    <form>
        <label>
            Enter your name (empty=random)<br/>
            <input class="smallbutton" type="text" name="seed" id="name" value="" autofocus/></label>
        <label>
            Avatar type
            <select name="type">
                <option value="cat" <?= $type === 'cat' ? 'selected="selected"' : '' ?>>Cat</option>
                <option value="bird" <?= $type === 'bird' ? 'selected="selected"' : '' ?>>Bird</option>
                <option value="mobilizon" <?= $type === 'mobilizon' ? 'selected="selected"' : '' ?>>Mobilizon</option>
                <option value="abstract" <?= $type === 'abstract' ? 'selected="selected"' : '' ?>>Abstract</option>
            </select>
        </label>
        <br/><br/>
        <input class="bigbutton" type="submit" value="Generate new avatar"/>
    </form>

    <em>(* empty=random)</em><br/><br/>
    A Free/Libre weekend project. 😺<br/><br/>
    <b>License:</b><br/>
    <b>Artwork:</b> <a href="https://creativecommons.org/licenses/by/4.0/">CC-By</a> David Revoy <a
            href="http://www.peppercarrot.com" title="my webcomic">[site]</a><br/>
    <b>Code:</b> <a href="https://en.wikipedia.org/wiki/MIT_License">MIT</a> Andreas Gohr <a
            href="https://www.splitbrain.org/blog/2007-01/20_monsterid_as_gravatar_fallback" title="original author">[site]</a><br/><br/>
    <b>Don't use my art for NFT! <a href="https://www.davidrevoy.com/article864/dream-cats-nfts-don-t-buy-them">Here is
            why.</a></b><br/><br/>
    <a href="https://framagit.org/Deevad/cat-avatar-generator" title="Project source code">[Source code]</a>&nbsp;
    &nbsp;<a href="https://www.davidrevoy.com/static3/become-my-patron" title="original author">[♥ Donate]</a>
</div>
</body>
</html>

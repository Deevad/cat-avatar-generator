###
# ====================
# AVATAR-GENERATOR
# ====================
#
# @authors: G. Bourel
#
# From initial PHP version of Andreas Gohr and David Revoy.
# 
# This Python script is licensed under the short and simple permissive:
# [MIT License](https://en.wikipedia.org/wiki/MIT_License)
# 
###

import random
import re
import sys
from PIL import Image
from os.path import abspath, dirname, exists  

# /!\ change the path to your system's cache or a folder(write permission) 
# Note: this path end with / and is relative to the cat-avatar-generator.php file.
cachepath = 'cache/';

MAX_SEED_LENGTH=80

def get_avatar(type, seed='default', cache= True, transparent = True):
    """
    Returns avatar image from provided seed, either for type cat or bird.
    """
    if type == 'cat':
        return get_cat(seed, cache, transparent)
    elif type == 'bird':
        return get_bird(seed, cache, transparent)
    else:
        raise AttributeError("Unkonwn type", type)

def get_cat(seed='default', cache= True, transparent = True):
    """
    Returns cat image from provided seed.
    """
    # init random seed
    random.seed(seed)

    # throw the dice for body parts
    parts = {
        'body': random.randrange(1,15),
        'fur': random.randrange(1,10),
        'eyes': random.randrange(1,15),
        'mouth': random.randrange(1,10),
        'accessory': random.randrange(1,20)
    }
    return create_avatar(seed, cache, transparent, 'cat', parts)

def get_bird(seed='default', cache= True, transparent = True):
    """
    Returns bird image from provided seed.
    """
    # init random seed
    random.seed(seed)

    # throw the dice for body parts
    parts = {
        'tail': random.randrange(1,10),
        'hoop': random.randrange(1,10),
        'body': random.randrange(1,10),
        'wing': random.randrange(1,10),
        'eyes': random.randrange(1,10),
        'beak': random.randrange(1,10),
        'accessory': random.randrange(1,20)
    }
    return create_avatar(seed, cache, transparent, 'bird', parts)

def create_avatar(seed, cache, transparent, type, parts):
    seed = slugify(str(seed))
    curdir = dirname(abspath(__file__))
    img_size = 70
    alpha = 0 if transparent else 1
    ext = 'png' if transparent else 'jpg'
    cache_file = f'{curdir}/cache/{type}_{seed}.{ext}'
    
    if len(seed) > MAX_SEED_LENGTH:
        raise AttributeError("Seed too long")
    if len(seed) == 0:
        raise AttributeError("Invalid seed")

    if cache and exists(cache_file):
        return Image.open(cache_file)

    # Create RGBA image 
    img  = Image.new( mode = "RGBA", size = (img_size, img_size), color = (255, 255, 255, alpha) )
    # add parts
    for part in parts:
        file = f'{curdir}/avatars/{type}/{part}_{parts[part]}.png'
        with Image.open(file) as im:
            img = Image.alpha_composite(img, im.convert("RGBA"))

    # Convert to RGB if no transparency
    if not transparent:
        img = img.convert("RGB")
    # Save into cache directory
    if cache:
        img.save(cache_file)
    return img

def slugify(val):
    """
    Normalizes string, removes non-alpha characters,
    and converts spaces to underscores.
    """
    val = re.sub(r'[^\w\s-]', '', val)
    return re.sub(r'[-\s]+', '_', val).strip('-_')


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print(r'2 arguments required : ')
        print(r' - type (either "cat" or "bird")')
        print(r' - seed value.')
    else:    
        seed = sys.argv[2]
        if sys.argv[1] == "cat":
            get_cat(seed)
        elif sys.argv[1] == "bird":
            get_bird(seed, transparent=False)
        else:
            print(f"Type unknown: {sys.argv[1]}")
